import 'dart:async';

import 'package:fit_kit/fit_kit.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String result = '';
  Map<DataType, List<FitData>> results = Map();
  bool permissions;

  DateTime get _dateFrom => new DateTime.now().subtract(new Duration(minutes: 10));
  DateTime get _dateTo => new DateTime.now();

  @override
  void initState() {
    super.initState();

    hasPermissions();
  }

  Future<void> read() async {
    results.clear();

    List<DataType> permissionList = new List();
    permissionList.add(DataType.HEART_RATE);
    try {
      permissions = await FitKit.requestPermissions(permissionList);
      if (!permissions) {
        result = 'requestPermissions: failed';
      } else {
          results[DataType.HEART_RATE] = await FitKit.read(
            DataType.HEART_RATE,
            dateFrom: _dateFrom,
            dateTo: _dateTo,
          );

        result = 'readAll: success';
      }
    } catch (e) {
      result = 'readAll: $e';
      print(e);
    }

    setState(() {});
  }


  Future<void> hasPermissions() async {
    try {
      //Asking for all permissions e.g. Heart Rate,Step Count,etc. at once
      permissions = await FitKit.hasPermissions(DataType.values);
    } catch (e) {
      result = 'hasPermissions: $e';
    }

    if (!mounted) return;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final items = results.entries.expand((entry) => [entry.key, ...entry.value]).toList();

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('FitKit Example'),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.symmetric(vertical: 8)),
              Text(
                  'Date Range: ${(_dateFrom)} - ${(_dateTo)}'),
              Text('Permissions: $permissions'),
              _buildButtons(context),
              Expanded(
                child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    final item = items[index];
                    if (item is DataType) {
                      return Padding(
                        padding: EdgeInsets.symmetric(vertical: 8),
                        child: Text(
                          '$item - ${results[item].length}',
                          style: Theme.of(context).textTheme.title,
                        ),
                      );
                    } else if (item is FitData) {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 4,
                          horizontal: 8,
                        ),
                        //Data is displayed from here.
                        child: Text(
                          '$item',
                          style: Theme.of(context).textTheme.caption,
                        ),
                      );
                    }

                    return Container();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _dateToString(DateTime dateTime) {
    if (dateTime == null) {
      return 'null';
    }

    return '${dateTime.day}.${dateTime.month}.${dateTime.year}';
  }

  Widget _buildButtons(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: FlatButton(
            color: Theme.of(context).accentColor,
            textColor: Colors.white,
            onPressed: () => read(),
            child: Text('Read'),
          ),
        ),
        Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
       Expanded(
         child: Container(),
       )
      ],
    );
  }
}